require 'byebug'
class Code
  attr_reader :pegs
  PEGS = {
            red: 'r',
            green: 'g',
            blue: 'b',
            yellow: 'y',
            orange: 'o',
            purple: 'p'
         }

  def self.parse(str)
    str = str.downcase
    unless str.chars.all? { |ch| PEGS.value?(ch) }
      raise 'invalid code'
    end
    Code.new(str.chars)
  end

  def self.random
    random_pegs = []
    4.times do
      random_pegs << PEGS.values.sample
    end
    Code.new(random_pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(comparison)
    count = 0
    pegs.each_with_index do |el, i|
      count += 1 if el == comparison.pegs[i]
    end
    count
  end

  def near_matches(comparison)
    matches = comparison.pegs.select { |el| pegs.include?(el) }
    matches.uniq.count - exact_matches(comparison)
  end

  def ==(comparison)
    return false if comparison.class != Code
    pegs == comparison.pegs
  end

end

class Game
  attr_reader :secret_code
  attr_accessor :guess_num

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
    @guess_num = 0
  end

  def play
    until guess_num == 10
      guess = get_guess
      if won?(guess)
        puts 'Congratulations, you win!'
        break
      end
      display_matches(guess)
    end
    puts "Sorry. You have no more guesses left. " +
         "The correct order was #{secret_code.pegs.join('')}"
  end

  def get_guess
    puts 'Please input your guess ex(RGBY):'
    input = gets.chomp
    @guess_num += 1
    Code.parse(input)
  end

  def display_matches(code)
    if code.exact_matches(secret_code) == 4
      puts 'Congratulations, you win!'
    else
      puts "You got #{code.exact_matches(secret_code)} exact matches " +
            "and #{code.near_matches(secret_code)} near matches"
    end
  end

  def won?(guess)
    guess == secret_code
  end

  if $PROGRAM_NAME == __FILE__
    puts 'Welcome to Mastermind'
    new_game = Game.new
    new_game.play
  end

end
